<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mail\BirthdaySendMessage;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Models\Contact\LogMailMessage;
use App\Models\Contact\Contacts;

class BirthDayNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:birth-day-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a message to someone whose birthday is today from contacts';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $list = Contacts::whereDay('birthday', NOW())->whereMonth('birthday', NOW())->orderBy('user_id')->get()->toArray();
        foreach ($list as $value) {
            $author = User::where('id', $value['user_id'])->first();
            $checked = LogMailMessage::where('contact_id', $value['id'])->whereYear('created_at', NOW());
            if ($checked->count() == 0){
                $fullName = null;

                if (!empty($value['first_name'])) {
                    $fullName = $value['first_name']. ' ';
                }
                if (!empty($value['last_name'])) {
                    $fullName .= $value['last_name']. ' ';
                }
                if (!empty($value['patronymic'])) {
                    $fullName .= $value['patronymic'];
                }

                $mailData = [
                    "title" => 'Сегодня день рождения у - '. $fullName
                ];

                Mail::to($author->email)->send(new BirthdaySendMessage($mailData));
                LogMailMessage::create([
                    'contact_id'    => $value['id'],
                    'email'         => $author->email,
                    'text'          => json_encode($mailData)
                ]);
            }
        }
    }
}
