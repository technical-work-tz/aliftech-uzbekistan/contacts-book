<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Contact\Contacts;
use App\Models\Contact\ContactNumbers;
use App\Models\Contact\ContactEmails;

class ContactController extends Controller
{
    protected function index(){
        $user = Auth::user();
        $contacts = Contacts::where('user_id', $user->id)
        ->select('id', 'photo', 'first_name', 'last_name', 'birthday')
        ->paginate();


        foreach ($contacts as $key => $value) {
            $contacts[$key]['phones'] = ContactNumbers::select('phone')->where('contact_id', $value['id'])->pluck('phone');
            $contacts[$key]['emails'] = ContactEmails::select('email')->where('contact_id', $value['id'])->pluck('email');
            $contacts[$key]['photo'] = env('APP_URL').'/storage/app/public/image/contacts/'.$user->id.'/'.$value->photo;
        }
        // RESPONSE
            return response()->json([
                'status' => true,
                'code' => 200,
                'data' => $contacts,
                'errors' => null,
            ], 200);
    }
}
