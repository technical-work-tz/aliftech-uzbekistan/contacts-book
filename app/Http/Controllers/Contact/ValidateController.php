<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use App\Models\Contact\Contacts;

class ValidateController extends Controller
{
    static function filter(Request $request, $update = false) {
        $user = Auth::user();
        $request->validate([
            'phone_number' => 'array',
            'email' => 'array',
            'photo' => 'file|mimes:jpg,png|max:20480',
            'first_name' =>  'string|min:1|max:255',
            'last_name' =>  'string|min:1|max:255',
            'patronymic' =>  'string|min:1|max:255',
            'birthday' => 'date_format:Y-m-d|before:today',
        ]);

        if (empty($request->phone_number) AND empty($request->email)){
            throw ValidationException::withMessages(['contact' => 'Enter phone number or email']);
        }

        if (empty($request->first_name) AND empty($request->last_name) AND empty($request->patronymic)){
            throw ValidationException::withMessages(['contact' => 'Write the first name, last name or patronymic']);
        }

        if ($update == false AND
            Contacts::where('first_name', $request->first_name)
            ->where('last_name', $request->last_name)
            ->where('patronymic', $request->patronymic)
            ->where('user_id', $user->id)->count() > 0
        ){
            throw ValidationException::withMessages(['contact' => 'Such a contact exists']);
        }
    }
}
