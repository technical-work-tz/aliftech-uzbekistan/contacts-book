<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Contact\ValidateController;


use App\Models\Contact\Contacts;
use App\Models\Contact\ContactNumbers;
use App\Models\Contact\ContactEmails;


class AddController extends Controller
{
    protected function create(Request $request){
        $user = Auth::user();

        // VALIDATED
            ValidateController::filter($request);

        // CREATE CONTACT
            if ($request->has('photo')) {
                $filePath = '/public/image/contacts/'.$user->id;
                $photo = uniqid() . '.' . $request->photo->extension();
                $request->photo->storeAs($filePath, $photo);
                $photoUrl = env('APP_URL').'/storage/app'.$filePath.'/'.$photo;
            }else{
                $photo = null;
                $photoUrl = null;
            }

            $contact = Contacts::create([
                'user_id'       => $user->id,
                'photo'         => $photo,
                'first_name'    => $request['first_name'],
                'last_name'     => $request['last_name'],
                'patronymic'    => $request['patronymic'],
                'birthday'      => $request['birthday'],
            ]);

        // CREATE PHONE NUMBER
            if (!empty($request->phone_number)){
                $phones = [];
                foreach ($request->phone_number as $phone_number) {
                    $phone_number = trim($phone_number, '+');
                    // CREATE
                        ContactNumbers::create([
                            'contact_id'    => $contact->id,
                            'phone'         => $phone_number
                        ]);
                        array_push($phones, $phone_number);
                }
            }

        // CREATE EMAIL
            if (!empty($request->email)){
                $emails = [];
                foreach ($request->email as $email) {
                    $email = trim($email);
                    // VALIDATED
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                            throw ValidationException::withMessages(['email' => 'Error, '.$email.' is not an email address']);
                        }

                    // CREATE
                        ContactEmails::create([
                            'contact_id'    => $contact->id,
                            'email'         => $email
                        ]);

                    array_push($emails, $email);
                }
            }


        // RESPONSE
            return response()->json([
                'status' => true,
                'code' => 201,
                'data' => [
                    'photo'         => $photoUrl,
                    'first_name'    => $request['first_name'],
                    'last_name'     => $request['last_name'],
                    'patronymic'    => $request['patronymic'],
                    'phones'        => $phones,
                    'emails'        => $emails,
                    'birthday'      => $request['birthday'],
                ],
                'errors' => null,
            ], 201);
    }
}
