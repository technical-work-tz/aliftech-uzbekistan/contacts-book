<?php
namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Contact\ValidateController;
use Illuminate\Support\Facades\Storage;

use App\Models\Contact\Contacts;
use App\Models\Contact\ContactNumbers;
use App\Models\Contact\ContactEmails;

class UpdateController extends Controller
{
    protected function update(Request $request, $id){
        $user = Auth::user();
        $contact = Contacts::find($id);
        if (!isset($contact->id)){
            throw ValidationException::withMessages(['contact' => 'Error, not contact']);
        }
        if ($contact->user_id !== $user->id){
            throw ValidationException::withMessages(['contact' => 'Error, this contact is not yours']);
        }

        // VALIDATED
            ValidateController::filter($request, true);

        // UPDATE CONTACT
            $filePath = '/public/image/contacts/'.$user->id;
            if (!empty($request->photo)){
                Storage::delete($filePath . $contact->photo);
                $photo = uniqid() . '.' . $request->photo->extension();
                $request->photo->storeAs($filePath, $photo);
            }else{
                $photo = $contact->photo;
            }

            $contact->user_id      = $user->id;
            $contact->photo        = $photo;
            $contact->first_name   = $request['first_name'];
            $contact->last_name    = $request['last_name'];
            $contact->patronymic   = $request['patronymic'];
            $contact->birthday     = $request['birthday'];
            $contact->update();

        // CREATE PHONE NUMBER
            if (!empty($request->phone_number)){
                $phones = [];
                foreach ($request->phone_number as $phone_number) {
                    $phone_number = trim($phone_number, '+');
                    // CREATE
                        if (ContactNumbers::where('phone', $phone_number)->count() == 0){
                            ContactNumbers::create([
                                'contact_id'    => $contact->id,
                                'phone'         => $phone_number
                            ]);
                        }
                        array_push($phones, $phone_number);
                }


                $list = ContactNumbers::select('phone')->where('contact_id', $id)->get()->toArray();
                foreach ($list as $value) {
                    if (in_array($value['phone'], $phones) === false){
                        ContactNumbers::where('phone', $value)->delete();
                    }
                }
            }else{
                $phones = null;
            }

        // CREATE EMAIL
            if (!empty($request->email)){
                $emails = [];
                foreach ($request->email as $email) {
                    $email = trim($email);
                    // VALIDATED
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                            throw ValidationException::withMessages(['email' => 'Error, '.$email.' is not an email address']);
                        }

                    // CREATE
                        if (ContactEmails::where('email', $email)->count() == 0){
                            ContactEmails::create([
                                'contact_id'    => $contact->id,
                                'email'         => $email
                            ]);
                        }

                    array_push($emails, $email);
                }


                $list = ContactEmails::select('email')->where('contact_id', $id)->get()->toArray();
                foreach ($list as $value) {
                    if (in_array($value['email'], $emails) === false){
                        ContactEmails::where('email', $value)->delete();
                    }
                }
            }else{
                $emails = null;
            }


        // RESPONSE
            return response()->json([
                'status' => true,
                'code' => 201,
                'data' => [
                    'photo'         => env('APP_URL').'/storage/app'.$filePath.'/'.$contact->photo,
                    'first_name'    => $request['first_name'],
                    'last_name'     => $request['last_name'],
                    'patronymic'    => $request['patronymic'],
                    'phones'        => $phones,
                    'emails'        => $emails,
                    'birthday'      => $request['birthday'],
                ],
                'errors' => null,
            ], 201);
    }
}
