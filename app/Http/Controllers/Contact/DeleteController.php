<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;


use App\Models\Contact\Contacts;
use App\Models\Contact\ContactNumbers;
use App\Models\Contact\ContactEmails;

class DeleteController extends Controller
{
    static function delete($id){
        $user = Auth::user();
        $contact = Contacts::find($id);
        if (!isset($contact->id)){
            throw ValidationException::withMessages(['contact' => 'Error, not contact']);
        }
        if ($contact->user_id !== $user->id){
            throw ValidationException::withMessages(['contact' => 'Error, this contact is not yours']);
        }

        ContactNumbers::where('contact_id', $contact->id)->delete();
        ContactEmails::where('contact_id', $contact->id)->delete();

        Storage::delete('/public/image/contacts/'.$user->id.'/'.$contact->photo);
        Contacts::where('id', $contact->id)->delete();


        return response()->json([
            'status' => true,
            'code' => 200,
            'data' => [
                'success' => true
            ],
            'errors' => null,
        ], 200);
    }
}
