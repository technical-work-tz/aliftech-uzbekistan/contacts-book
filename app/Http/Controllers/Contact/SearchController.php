<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    static function index(String $text){
        $user = Auth::user();

        // VALIDATED
            $validated = validator(
            [
                'text' => $text
            ],
            [
                'text' =>  [
                    'required',
                    'string',
                    'min:3',
                    'max:255'
            ],
            ])->validate();

            $data = DB::table('contacts')
            ->where('contacts.user_id', $user->id)
            ->where('first_name', 'LIKE', '%' . $validated['text'] . '%')
            ->orWhere('last_name', 'LIKE', '%' . $validated['text'] . '%')
            ->orWhere('patronymic', 'LIKE', '%' . $validated['text'] . '%')
            ->leftJoin('contact_numbers', 'contacts.id', '=', 'contact_id')
            ->orWhere('phone', 'LIKE', '%' . $validated['text'] . '%')
            ->leftJoin('contact_emails', 'contacts.id', '=', 'contact_emails.contact_id')
            ->orWhere('email', 'LIKE', '%' . $validated['text'] . '%')
            ->select('contacts.photo','contacts.first_name','contacts.last_name','contacts.birthday', 'contact_numbers.phone', 'contact_emails.email')
            ->paginate();

        // RESPONSE
            return response()->json([
                'status' => true,
                'code' => 200,
                'data' => $data,
                'errors' => null,
            ], 200);
    }
}
