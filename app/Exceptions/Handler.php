<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    public function render($request, Throwable $exception)
    {
        // Abort
            if($exception instanceof NotFoundHttpException){
                return response()->json([
                    'status'  => false,
                    'code'    => $exception->getStatusCode(),
                    'data' => null,
                    'errors'  => $exception->getMessage(),
                ], $exception->getStatusCode());
            }

            if($exception instanceof HttpException){
                return response()->json([
                    'status'  => false,
                    'code'    => $exception->getStatusCode(),
                    'data' => null,
                    'errors'  => $exception->getMessage(),
                ], $exception->getStatusCode());
            }

        return parent::render($request, $exception);
    }

    // VALIDATE
        protected function invalidJson($request, ValidationException $exception)
        {
            return response()->json([
                'status'  => false,
                'code'    => $exception->status,
                'data' => null,
                'errors'  => $this->transformErrors($exception),
            ], $exception->status);
        }

        private function transformErrors(ValidationException $exception)
        {
            $errors = [];

            foreach ($exception->errors() as $field => $message) {
            $errors[] = [
                'field' => $field,
                'message' => $message[0],
            ];
            }

            return $errors;
        }
}
