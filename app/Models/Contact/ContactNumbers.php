<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactNumbers extends Model
{
    use HasFactory;

    protected $fillable = [
        'contact_id',
        'phone'
    ];
}
