<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Contact\ContactNumbers;
use App\Models\Contact\ContactEmails;

class Contacts extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'photo',
        'first_name',
        'last_name',
        'patronymic',
        'birthday'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
