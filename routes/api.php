<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\Contact\ContactController;
use App\Http\Controllers\Contact\AddController;
use App\Http\Controllers\Contact\UpdateController;
use App\Http\Controllers\Contact\DeleteController;
use App\Http\Controllers\Contact\SearchController;


// USER
    Route::post('user/registration', [AuthController::class, 'register']);
    Route::post('user/login', [AuthController::class, 'login']);

// CONTACT
    Route::prefix('contacts')->middleware(['auth:api'])->group(function() {
        Route::get('index', [ContactController::class, 'index']);
        Route::post('add', [AddController::class, 'create']);
        Route::put('edit/{id}', [UpdateController::class, 'update']);
        Route::delete('delete/{id}', [DeleteController::class, 'delete']);
        Route::get('search/{text}', [SearchController::class, 'index']);
    });
