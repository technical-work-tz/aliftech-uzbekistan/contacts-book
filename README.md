# Здравствуйте 👋
#### Контактная книжка
![The San Juan Mountains are beautiful!](https://ouch-cdn2.icons8.com/i5jmC1ahLKIyoxxvs3Rw_GMuejy7IbRRSqALWm9tl_M/rs:fit:256:256/czM6Ly9pY29uczgu/b3VjaC1wcm9kLmFz/c2V0cy9wbmcvMzE4/L2UzZTQ4NDg2LWIx/YTEtNGIwMS1hNjEw/LTVlYzdlMzAyNjA2/ZC5wbmc.png "San Juan Mountains")

### Для проекта использовано
<ul>
  <li>PHP (8.2)</li>
  <li>Laravel (10)</li>
  <li>NGINX (1.14)</li>
  <li>MySQL (8.0)</li>
  <li>phpMyAdmin (5.1.2)</li>
  <li>composer</li>
  <li>Docker</li>
</ul>

### Установка по порту
<ul>
  <li>php (:9000)</li>
  <li>NGINX (:8080)</li>
  <li>MySQL (:3306)</li>
  <li>phpMyAdmin (:8081)</li>
</ul>

### Команды что бы запустить проект
#### запустить Docker
> docker compose up -d --build app

#### установить composer
> docker compose run --rm composer update

#### миграции база даныых
> docker compose run --rm artisan migrate

#### установить Laravel Passport и получить генерирование ключи
> docker compose run --rm artisan passport:install --force



#### Данные для phpMyAdmin
> - Login: aliftech
> - Password: JTPwSYBoLnNckcaxy

### Документация по API
[API DOCUMENTATION](https://documenter.getpostman.com/view/15553314/2s93Xzvgf7)

